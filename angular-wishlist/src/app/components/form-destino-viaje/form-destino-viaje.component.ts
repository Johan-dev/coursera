import { Component, OnInit, Output, EventEmitter, forwardRef, Inject } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;

  fg: FormGroup;
  minLongitudNom = 3;
  searchResult: string[];

  constructor(fb: FormBuilder,@Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['',Validators.compose([
        Validators.required,
        this.validadorParametrizable(this.minLongitudNom)
      ])],
      url: ['']
    });

    this.fg.valueChanges.subscribe((form: any)=>{
      console.log('cambio realizado',form);
    });
  }

  ngOnInit(): void {
    let elementNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elementNombre, 'input').pipe
    (
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
    ).subscribe(ajaxResponse => this.searchResult = ajaxResponse.response);
  }

  guardar(nombre:string,url:string): boolean{
    let item = new DestinoViaje(nombre,url);
    this.onItemAdded.emit(item);
    return false;
  }

  nombreValidador(control: FormControl): { [s: string]: boolean }
  {
    let longitud = control.value.toString().trim().length;
    if(longitud > 0 && longitud < this.minLongitudNom){
      return { invalidNombre:true }
    }
    return null;
  }

  validadorParametrizable(minLongitudNom:number): ValidatorFn{
    return(control: FormControl): { [s: string]: boolean } | null => {
      let longitud = control.value.toString().trim().length;
      if(longitud > 0 && longitud < this.minLongitudNom){
        return { minLongNombre:true }
      }
      return null;
    }
  }
}
