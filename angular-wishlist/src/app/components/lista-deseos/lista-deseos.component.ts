import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.models';
import { DestinoViaje } from "../../models/destino-viaje.model";
import { DestinosApiClient} from "../../models/destinos-api-client.model";


@Component({
  selector: 'app-lista-deseos',
  templateUrl: './lista-deseos.component.html',
  styleUrls: ['./lista-deseos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDeseosComponent implements OnInit 
{
  @Output() onItemAdded:EventEmitter<DestinoViaje>;
  updates: string[];
  all;
  constructor(public destinosApiClient:DestinosApiClient, public store: Store<AppState>) 
  {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
  }

  ngOnInit()
  {
    this.store.select(state => state.destinos.favorito)
    .subscribe(data => {
      const f = data;
      if (f != null) {
        this.updates.push('Se eligió: ' + f.nombre);
      }
    });
  }

  agregado(item: DestinoViaje)
  {
    this.destinosApiClient.add(item);
    this.onItemAdded.emit(item);
  }

  elegido(item:DestinoViaje){
    this.destinosApiClient.elegir(item);
  }
  
}
